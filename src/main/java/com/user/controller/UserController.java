package com.user.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user")
@Api(value = "UserController",tags = "用户管理-相关接口")
public class UserController {

    @ApiOperation(value = "登陆",notes = "登陆")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "username",value = "用户名",required = true,paramType = "query"),
            @ApiImplicitParam(name = "password",value = "密码",required = true,paramType = "query")
    })
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    @ResponseBody
    public String Login(){
        return "true";
    }
}
