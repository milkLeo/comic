package com;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.function.Predicate;

import static springfox.documentation.builders.PathSelectors.regex;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .paths(Predicates.not(PathSelectors.regex("/error.*")))
                .build()
                //.globalOperationParameters(params())
                ;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("comic project 接口集合 ")
                .description("")
                .termsOfServiceUrl("XXXXXXXXXXXX")
                .contact("陪你度过漫长的岁月")
                .version("1.0")
                .build();
    }
    //可以不需要---这是用来需要登录cookie和tooken验证的
   /* private List<Parameter> params() {
        List<Parameter> pars = new ArrayList<>();
        ParameterBuilder ticketPar = new ParameterBuilder();
        ParameterBuilder contentType = new ParameterBuilder().name("Content-Type").defaultValue("application/json").modelRef(new ModelRef("string")).parameterType("header");
        ticketPar.name("Cookie").description("user ticket").defaultValue("TICKET=")
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(false).build();

        pars.add(ticketPar.build());
        return pars;
    }*/

}
